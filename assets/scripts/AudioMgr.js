cc.Class({
    extends: cc.Component,

    properties: {
        musicSource: cc.AudioSource,
        soundSource1: cc.AudioSource,
        soundSource2: cc.AudioSource,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.musicSource.volume = 0.3;
        // this.soundSource.volume = 0.7;
    },

    // update (dt) {},

    PlaySound(soundName){
        var self = this;
        cc.loader.load(cc.url.raw("resources/audio/" + soundName + ".mp3"), function (err, source) {
            var soundSource = self.soundSource1.isPlaying == false ? self.soundSource1 : self.soundSource2;
            soundSource.clip = source;
            soundSource.play();
        });
    },
});
